import java.util.ArrayList;
import java.util.Random;

public class UnoDeck {
    protected ArrayList<ICard> deck = new ArrayList<ICard>();

    public UnoDeck(){
      
        //makes the special cards and wild cards
        for(int i = 0; i < 4; i++){
            String color = "";
            switch(i){
                case 0:
                    color = "Red";
                    break;
                case 1:
                    color = "Green";
                    break;
                case 2:
                    color = "Blue";
                    break;
                case 3:
                    color = "Yellow";
                    break;
            }
            wildCard  wildCard_add = new wildCard();
            this.addToDeck(wildCard_add);
            wildpickup4Card wildCardpickup4_add = new wildpickup4Card();
            this.addToDeck(wildCardpickup4_add);

            skipCard skipCard1 = new skipCard(color);
            this.addToDeck(skipCard1);
            reverseCard reverseCard1 = new reverseCard(color);
            this.addToDeck(reverseCard1);
            pickup2Card pickup2Card1 = new pickup2Card(color);
            this.addToDeck(pickup2Card1);
            
        }

        //makes every numbered card
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 11; j++) {
                if (i == 0){
                    Numbered_Cards card_add = new Numbered_Cards("Red", j);
                    this.addToDeck(card_add);
                    this.addToDeck(card_add);
                }
                if (i == 1){
                    Numbered_Cards card_add = new Numbered_Cards("Green", j);
                    this.addToDeck(card_add);
                    this.addToDeck(card_add);

                }
                if (i == 2){
                    Numbered_Cards card_add = new Numbered_Cards("Blue", j);
                    this.addToDeck(card_add);
                    this.addToDeck(card_add);

                }
                if (i == 3){
                    Numbered_Cards card_add = new Numbered_Cards("Yellow", j);
                    this.addToDeck(card_add);
                    this.addToDeck(card_add);
                }
            }
            
        }
    }

    public void addToDeck(ICard card){
        deck.add(card);
    }

    public ICard draw(){
        ICard returnCard = deck.get(0);
        deck.remove(0);
        return returnCard;
    }
    public ArrayList<ICard> getArrayList(){
        return this.deck;
    }
    public void shuffle(){
        int deckSize = deck.size();
        Random randObj = new Random();

        for (int i = 0; i < deckSize; i++) {
            int switchingValue = randObj.nextInt(deckSize);
            //stores the cards in a variable
            ICard card1 = deck.get(i);
            ICard card2 = deck.get(switchingValue);
            //switches the two cards
            deck.set(i, card2);
            deck.set(switchingValue, card1);
        }
    }

}

public abstract class ASpecial_Cards implements ICard{
    protected String color;

    public ASpecial_Cards(String color){
        this.color = color;
    }

    public String getColor(){
        return this.color;
    }

    public boolean canPlay(ICard previousCard){
        boolean returnBool = false;

        //checks for the wildcards
        if (previousCard instanceof AWildCards){
            if(((AWildCards)previousCard).getColor() == this.color){
                 returnBool = true;
            } 
        }

        //checks for the numbered cards
        else if(previousCard instanceof Numbered_Cards){
             if((((Numbered_Cards)previousCard).getColor() == this.color)){
                 returnBool = true;
             }
        }

        //checks the special cards
        else if (previousCard instanceof ASpecial_Cards){
            if(((ASpecial_Cards)previousCard).getColor() == this.color){
                returnBool = true;
            }
        }

       return returnBool;
    }
}
import static org.junit.jupiter.api.Assertions.*;

import org.junit.Test;
public class TestWildCards {
    @Test
    public void testCanPlayTrue(){
        Numbered_Cards card1 = new Numbered_Cards("Red", 0);
        wildCard card2 = new wildCard();

        boolean assertReturn = card2.canPlay(card1);

        assertEquals(true, assertReturn);
    }

    @Test
    //tests the set and get color
    public void testSetAndGetColor(){
        wildCard card = new wildCard();
        card.setColor("Red");
        String color = card.getColor();

        assertEquals("Red", color);
    }

    @Test
    public void testCanPlayWildPickup4True(){
        Numbered_Cards card1 = new Numbered_Cards("Red", 0);
        wildpickup4Card card2 = new wildpickup4Card();

        boolean assertReturn = card2.canPlay(card1);

        assertEquals(true, assertReturn);
    }

    @Test
    public void testSetAndGetColorWildPickup4(){
        wildpickup4Card card = new wildpickup4Card();
        card.setColor("Red");
        String color = card.getColor();

        assertEquals("Red", color);
    }


}

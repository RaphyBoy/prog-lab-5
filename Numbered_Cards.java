public class Numbered_Cards implements ICard{
    protected int number;
    protected String color;

    public Numbered_Cards(String color,int number){
        this.number = number;
        this.color = color;
    }
    public String getColor(){
        return this.color;
    }
    public int getNumber(){
        return this.number;
    }

    public boolean canPlay(ICard previousCard){
        boolean returnBool = false;
        
        //checks for the wildcards
       if (previousCard instanceof AWildCards){
           if(((AWildCards)previousCard).getColor() == this.color){
                returnBool = true;
           } 
       }

       //checks for the numbered cards
       else if(previousCard instanceof Numbered_Cards){
            if((((Numbered_Cards)previousCard).getColor() == this.color) || (((Numbered_Cards)previousCard).getNumber() == this.number)){
                returnBool = true;
            }
       }
       
       //checks the special cards
       else if (previousCard instanceof ASpecial_Cards){
           if(((ASpecial_Cards)previousCard).getColor() == this.color){
               returnBool = true;
           }
       }
       
       return returnBool;
    }
}
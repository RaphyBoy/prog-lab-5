import static org.junit.jupiter.api.Assertions.*;

import org.junit.Test;

public class TestNumbered_Cards {
    @Test
    public void testGetColor(){
        Numbered_Cards card = new Numbered_Cards("Red", 0);

        String color = card.getColor();
        assertEquals("Red", color);
    }

    @Test
    public void testGetNumber(){
        Numbered_Cards card = new Numbered_Cards("Red", 0);

        int number = card.getNumber();
        assertEquals(0, number);
    }

    @Test
    public void testCanPlayNumberedCardsTrue(){
        Numbered_Cards card1 = new Numbered_Cards("Red", 0);
        Numbered_Cards card2 = new Numbered_Cards("Red", 1);
        Boolean playableCard = card1.canPlay(card2);
        assertEquals(true, playableCard);
    }

    @Test
    public void testCanPlayNumberedCardsFalse(){
        Numbered_Cards card1 = new Numbered_Cards("Red", 0);
        Numbered_Cards card2 = new Numbered_Cards("Yellow", 1);
        Boolean playableCard = card1.canPlay(card2);
        assertEquals(false, playableCard);
    }

    @Test
    public void testCanPlayWildCardsTrue(){
        Numbered_Cards card1 = new Numbered_Cards("Red", 0);
        wildCard card2 = new wildCard();
        card2.setColor("Red");
        Boolean playableCard = card1.canPlay(card2);
        assertEquals(true, playableCard);
    }

    @Test
    public void testCanPlayWildCardsFalse(){
        Numbered_Cards card1 = new Numbered_Cards("Red", 0);
        wildCard card2 = new wildCard();
        card2.setColor("Yellow");
        Boolean playableCard = card1.canPlay(card2);
        assertEquals(false, playableCard);
    }

    @Test
    public void testCanPlaySpecialCardsTrue(){
        Numbered_Cards card1 = new Numbered_Cards("Red", 0);
        reverseCard card2 = new reverseCard("Red");
        Boolean playableCard = card1.canPlay(card2);
        assertEquals(true, playableCard);
    }

    @Test
    public void testCanPlaySpecialCardsFalse(){
        Numbered_Cards card1 = new Numbered_Cards("Red", 0);
        reverseCard card2 = new reverseCard("Yellow");
        Boolean playableCard = card1.canPlay(card2);
        assertEquals(false, playableCard);
    }
}

public interface ICard{
    boolean canPlay(ICard previousCard);
}
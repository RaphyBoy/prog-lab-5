import static org.junit.jupiter.api.Assertions.*;

import org.junit.Test;
public class TestSpecialCards {
    @Test
    public void testReverseCardGetColor(){
        reverseCard card = new reverseCard("Red");
        String color = card.getColor();

        assertEquals("Red", color);
    }

    @Test
    public void testReverseCardCanPlayTrue(){
        reverseCard card1 = new reverseCard("Red");
        reverseCard card2 = new reverseCard("Red");

        boolean assertReturn = card1.canPlay(card2);

        assertEquals(true, assertReturn);
    }

    @Test
    public void testReverseCardCanPlayFalse(){
        reverseCard card1 = new reverseCard("Red");
        reverseCard card2 = new reverseCard("Yellow");

        boolean assertReturn = card1.canPlay(card2);

        assertEquals(false, assertReturn);
    }

    @Test
    public void testSkipCardCanPlayTrue(){
        reverseCard card1 = new reverseCard("Red");
        skipCard card2 = new skipCard("Red");

        boolean assertReturn = card1.canPlay(card2);

        assertEquals(true, assertReturn);
    }

    @Test
    public void testSkipCardCanPlayFalse(){
        reverseCard card1 = new reverseCard("Red");
        skipCard card2 = new skipCard("Yellow");

        boolean assertReturn = card1.canPlay(card2);

        assertEquals(false, assertReturn);
    }

    @Test
    public void testPickup2CardCanPlayTrue(){
        reverseCard card1 = new reverseCard("Red");
        pickup2Card card2 = new pickup2Card("Red");

        boolean assertReturn = card1.canPlay(card2);

        assertEquals(true, assertReturn);
    }

    @Test
    public void testPickup2CardCanPlayFalse(){
        reverseCard card1 = new reverseCard("Red");
        pickup2Card card2 = new pickup2Card("Yellow");

        boolean assertReturn = card1.canPlay(card2);

        assertEquals(false, assertReturn);
    }

    @Test
    public void testNumberedCardsCanPlayTrue(){
        reverseCard card1 = new reverseCard("Red");
        Numbered_Cards card2 = new Numbered_Cards("Red",0);

        boolean assertReturn = card1.canPlay(card2);

        assertEquals(true, assertReturn);
    }

    @Test
    public void testNumberedCardsCanPlayFalse(){
        reverseCard card1 = new reverseCard("Red");
        Numbered_Cards card2 = new Numbered_Cards("Yellow",0);

        boolean assertReturn = card1.canPlay(card2);

        assertEquals(false, assertReturn);
    }

    @Test
    public void testWildCardsCanPlayTrue(){
        reverseCard card1 = new reverseCard("Red");
        wildCard card2 = new wildCard();
        card2.setColor("Red");

        boolean assertReturn = card1.canPlay(card2);

        assertEquals(true, assertReturn);
    }

    @Test
    public void testWildCardsCanPlayFalse(){
        reverseCard card1 = new reverseCard("Red");
        wildCard card2 = new wildCard();
        card2.setColor("Yellow");

        boolean assertReturn = card1.canPlay(card2);

        assertEquals(false, assertReturn);
    }
}

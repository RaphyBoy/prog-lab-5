import static org.junit.jupiter.api.Assertions.*;

import org.junit.Test;

public class TestUnoDeck {

    @Test
    public void DeckLength(){
        UnoDeck deck = new UnoDeck();
        int length = deck.getArrayList().size();

        assertEquals(108, length);
    }
    @Test
    public void testShuffle(){
        //it is possible that it fails simply because the
        //algorithm might not change this specific slot of the array
        //so if u run again it will most likely work

        UnoDeck deck = new UnoDeck();
        ICard cardBeforeShuffle = deck.getArrayList().get(0);
        deck.shuffle();
        ICard cardAfterShuffle = deck.getArrayList().get(0);

        boolean assertValue = false;
        if (cardBeforeShuffle.equals(cardAfterShuffle)){
            //means its the same 
            assertValue = true;
        }

        assertEquals(false, assertValue);
    }

    @Test
    public void testAddToDeck(){
        //tests the addtodeck method 
        UnoDeck deck = new UnoDeck();
        wildCard card = new wildCard(); 
    
        deck.addToDeck(card);

        int arrayListIndex = deck.getArrayList().size(); 


        assertEquals(card, deck.getArrayList().get(arrayListIndex-1));
    }

    @Test 
    public void testDraw(){
        UnoDeck deck = new UnoDeck();

        //puts the card in a variable
        ICard card = deck.getArrayList().get(0);
        //puts the card in a variable and removes it from the array
        ICard cardDrew = deck.draw();

        assertEquals(card, cardDrew);
    }
}
